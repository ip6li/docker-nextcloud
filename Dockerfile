from debian:bookworm-slim

ARG NC_HOSTNAME
ARG NC_DOMAIN
ARG NC_DATADIR

run DEBIAN_FRONTEND=noninteractive apt-get update
run DEBIAN_FRONTEND=noninteractive apt-get install -q -y apt-utils
run DEBIAN_FRONTEND=noninteractive apt-get install -q -y \
  locales

RUN sed -i -e 's/# de_DE.UTF-8 UTF-8/de_DE.UTF-8 UTF-8/' /etc/locale.gen && \
    locale-gen
ENV LANG de_DE.UTF-8
ENV LANGUAGE de_DE:de
ENV LC_ALL de_DE.UTF-8

run DEBIAN_FRONTEND=noninteractive apt update && apt -q -y full-upgrade

# Install Postfix.
run echo "postfix postfix/main_mailer_type string Internet site" > preseed.txt
run echo "postfix postfix/mailname string ${NC_HOSTNAME}.${NC_DOMAIN}" >> preseed.txt
# Use Mailbox format.
run debconf-set-selections preseed.txt
run DEBIAN_FRONTEND=noninteractive apt-get install -q -y \
  postfix

run DEBIAN_FRONTEND=noninteractive apt-get install -q -y \
  procps \
  cron \
  curl \
  lsb-release \
  ca-certificates \
  bzip2 \
  git \
  ffmpeg \
  libmagickcore-6.q16-6-extra

run DEBIAN_FRONTEND=noninteractive apt-get install -q -y --no-install-recommends \
  libreoffice-core

run \
  curl -sSLo /usr/share/keyrings/deb.sury.org-php.gpg https://packages.sury.org/php/apt.gpg && \
  sh -c 'echo "deb [signed-by=/usr/share/keyrings/deb.sury.org-php.gpg] https://packages.sury.org/php/ $(lsb_release -sc) main" > /etc/apt/sources.list.d/php.list' && \
  DEBIAN_FRONTEND=noninteractive apt-get update

run DEBIAN_FRONTEND=noninteractive apt-get install -q -y \
  libapache2-mod-php8.3 \
  php-pear \
  php8.3-apcu \
  php8.3-imagick \
  php8.3-memcached \
  php8.3-redis \
  php8.3-pgsql \
  php8.3-mysql \
  php8.3-zip \
  php8.3-bz2 \
  php8.3-mbstring \
  php8.3-gd \
  php8.3-curl \
  php8.3-intl \
  php8.3-bcmath \
  php8.3-gmp

#run rm -rf /etc/apache2 /etc/php /etc/postfix /var/www/html
run \
  for i in /etc/apache2 /etc/php /etc/postfix /var/www/html; do mv ${i} ${i}.ORIG; done

COPY ./entrypoint.sh /root/entrypoint.sh
RUN chmod 700 /root/entrypoint.sh
ENTRYPOINT [ "/root/entrypoint.sh" ]


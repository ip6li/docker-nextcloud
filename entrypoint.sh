#!/usr/bin/env bash

FQDN="${NC_HOSTNAME}.${NC_DOMAIN}"

cd /var/www/html || exit 1
if [ ! -d nextcloud ]; then
   curl -O https://download.nextcloud.com/server/releases/latest.tar.bz2 || exit 1
   tar xvf latest.tar.bz2 || exit 1
   rm -f latest.tar.bz2
   cd ..
   if [ ! -d nextcloud-data ]; then
     mkdir nextcloud-data
   fi

   cp -a /run/secrets/autoconfig.php /var/www/html/nextcloud/config/
   chown -R www-data:www-data .
fi

echo "ServerName ${NC_HOSTNAME}.${NC_DOMAIN}" > /etc/apache2/conf-available/servername.conf
if [ ! -f /etc/apache2/conf-enabled/servername.conf ]; then
  cd /etc/apache2/conf-enabled && ln -s ../conf-available/servername.conf
fi


chown www-data:crontab /var/spool/cron/crontabs/www-data \
  && chmod 600 /var/spool/cron/crontabs/www-data

/usr/sbin/cron

/usr/sbin/postfix start

/usr/sbin/apachectl -D FOREGROUND


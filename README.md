# Nextcloud Docker

A very minimalistic and configurable Nextcloud solution on Docker.

# Configuration

You have to set up need two files:

* autoconfig.php
* .env

NC_DATADIR in .env must exactly match "directory" in autoconfig.php

# Database

You need an external database. Image supports PostgreSQL and MariaDB.

# Postgres

Setup PostgreSQL database and user.

```
create database nextcloud;
create user nextcloud password 'A*VERY*BAD*PASSWORD';
grant all on database nextcloud to nextcloud;
alter database nextcloud owner to nextcloud;
```

# MariaDB

*tbd*

# E-Mail

This image contains a local Postfix installation which is much more flexible than
PHP SMTP implementation. It is recommended to set Nextcloud SMTP to 127.0.0.1:53
and configure Postfix to to real SMTP handling. Either set up Postfix to use
a real mail provider using SASL authentication or send mail directly.

# State

pre-alpha

# Sources

This image is based on following sources:

* Debian slim (official Debian reposiotory)
* Nextcloud official distribution file
* sury.org: PHP packages for Debian

